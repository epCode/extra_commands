local S = minetest.get_translator("mcl_commands")

minetest.register_chatcommand("throw", {
	params = ("<name> <X>,<Y>,<Z>"),
	description = ("Add velocity to player. If put in the arrangement: [ <name> look <number> ] then it will throw the person in the direction they are looking at the speed of the second number"),
	privs = {server=true},
	func = function(name, param)
    input = {}
    for w in param:gmatch("%S+") do
      table.insert(input, w)
    end
    if input[4] then
      if tonumber(input[2]) ~= nil and tonumber(input[3]) ~= nil and tonumber(input[4]) ~= nil then
        player = minetest.get_player_by_name(input[1])
        if player ~= nil then
          player:add_velocity({x=tonumber(input[2]), y=tonumber(input[3]), z=tonumber(input[4])})
      		minetest.chat_send_player(name, input[1].." thrown")
        else
          minetest.chat_send_player(name, "invalid player name")
        end
      else
        minetest.chat_send_player(name, "Asked for number, got string")
      end
    elseif input[3] and input[2] == "look" and tonumber(input[3]) then
      player = minetest.get_player_by_name(input[1])
      local look_dir = player:get_look_dir()
      if player ~= nil then
        player:add_velocity(vector.multiply(look_dir, tonumber(input[3])))
        minetest.chat_send_player(name, input[1].." thrown")
      else
        minetest.chat_send_player(name, "invalid player name")
      end
    else
      minetest.chat_send_player(name, "missing parameters")
    end
	end,
})

minetest.register_chatcommand("damage", {
	params = ("<name> <amount>"),
	description = ("Add velocity to player"),
	privs = {server=true},
	func = function(name, param)
    input = {}
    for w in param:gmatch("%S+") do
      table.insert(input, w)
    end
    if input[2] then
      player = minetest.get_player_by_name(input[1])
      if player ~= nil then
        player:set_hp(player:get_hp() - tonumber(input[2]))
        minetest.chat_send_player(name, input[1].." damaged")
      else
        minetest.chat_send_player(name, "invalid player name")
      end
    else
      minetest.chat_send_player(name, "missing parameters")
    end

	end,
})
